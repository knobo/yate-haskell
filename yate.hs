
import Network
import System.IO
import Control.Exception
import Control.Concurrent

threaded = bracket
           (listenOn $ PortNumber 8000)
           (sClose)
           (loop)
    where loop sock = accept sock >>= handle >> loop sock
          handle (h, n, p) = 
              forkIO (hPutStrLn h "hi!" >> hClose h)




main = threaded
